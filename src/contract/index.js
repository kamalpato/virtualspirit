const firstImage = require('../../assets/images/a.png');
const secondImage = require('../../assets/images/b.png');
const thirdImage = require('../../assets/images/c.png');

export const dataLabel = [
    {
        id: 0,
        label: "Like All"
    },
    {
        id: 1,
        label: "Reset All"
    },
    {
        id: 2,
        label: "Dislike All"
    }
];

export const dataContent = [
    {
      id: 0,
      image: firstImage,
    },
    {
      id: 1,
      image: secondImage,
    },
    {
      id: 2,
      image: thirdImage,
    },
  ]


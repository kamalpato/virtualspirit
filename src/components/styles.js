import { StyleSheet } from "react-native"

export const ComponentStyles = (id) => StyleSheet.create({
    TouchableItem: {
        width: 100,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor:
            id == 0 ?
                "#2B72C4" :
                id == 1 ? "#FFF"
                    : "#DA2C2C"
    },
    LabelItem: {
        color: id == 1 ? "#000" : "#FFF"
    },
    ContentContainer: {
        backgroundColor: "#FFF",
        marginHorizontal: 25,
        marginVertical: 10,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
    },
    ImageContent: {
        height: 200,
        width: '100%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    ButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 10,
        paddingVertical: 10
    },
    CountView: { width: '50%' },
    CountLike: {
        width: 100,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        borderWidth: 1,
        borderColor: "#D5D5D5",
    },
    ConditionalContainer: {
        width: '50%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 20
    },
    LikeButton: {
        width: 70,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: '#2B72C4',
        marginRight: 10
    },
    LikeLabel: { color: "#FFF" },
    DislikeButton: {
        width: 70,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: '#DA2C2C',
    },
    DislikeLabel: { color: "#FFF" }
})
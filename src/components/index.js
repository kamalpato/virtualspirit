import React from "react"
import { TouchableOpacity, Text, View, Image } from "react-native"
import { ComponentStyles } from './styles';


export const renderItem = (data) => {
    return data.map((item, id) => (
        <TouchableOpacity style={ComponentStyles(id).TouchableItem}>
            <Text style={ComponentStyles(id).LabelItem}>{item.label}</Text>
        </TouchableOpacity>
    ))
}

export const renderContent = (data) => {
    return data.map((item, id) => (
        <View style={ComponentStyles(id).ContentContainer}>
            <View >
                <Image source={item.image} style={ComponentStyles(id).ImageContent} />
            </View>
            <View style={ComponentStyles(id).ButtonContainer}>
                <View style={ComponentStyles(id).CountView}>
                    <TouchableOpacity style={ComponentStyles(id).CountLike}>
                        <Text>100 Like</Text>
                    </TouchableOpacity>
                </View>
                <View style={ComponentStyles(id).ConditionalContainer}>
                    <TouchableOpacity style={ComponentStyles(id).LikeButton}>
                        <Text style={ComponentStyles(id).LikeLabel}>Like</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={ComponentStyles(id).DislikeButton}>
                        <Text style={ComponentStyles(id).DislikeLabel}>Dislike</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    ))
}
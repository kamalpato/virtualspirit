import { StyleSheet } from "react-native"

export const AppStyles = StyleSheet.create({
    SafeArea: {
        flex: 1,
        backgroundColor: '#F4F4F4'
    },
    FirstContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 25,
        marginVertical: 20
    }
})
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, { useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  Image
} from 'react-native';

import { dataLabel, dataContent } from './src/contract';
import { AppStyles } from './AppStyle';
import { ComponentStyles } from './src/components/styles';

const App = () => {
  const [countLike, setCountLike] = useState(Array(dataContent.length).fill(0))

  const onPressAll = (id) => {
    if (id == 0) {
      likeAllPressed(id)
    } else if (id == 1) {
      allReseted()
    } else {
      dislikeAllPressed(id)
    }
  }

  const allReseted = () => {
    setCountLike(prevCountLike => prevCountLike.map(() => 0));
  }

  const likeAllPressed = () => {
    setCountLike(prevCountLike => {
      const updatedCountLike = [...prevCountLike];
      updatedCountLike.forEach((count, index) => {
        updatedCountLike[index] = count + 1;
      });
      return updatedCountLike;
    });
  }

  const dislikeAllPressed = () => {
    setCountLike(prevCountLike => {
      const updatedCountLike = [...prevCountLike];
      updatedCountLike.forEach((count, index) => {
        if (count > 0) {
          updatedCountLike[index] = count - 1;
        }
      });
      return updatedCountLike;
    });
  }

  const likePressed = (id) => {
    setCountLike((prevCountLike) => {
      const updatedCountLike = [...prevCountLike];
      updatedCountLike[id] += 1;
      return updatedCountLike;
    });
  }

  const dislikePressed = (id) => {
    if (countLike == 0) {
      return 0
    } else {
      setCountLike((prevCountLike) => {
        const updatedCountLike = [...prevCountLike];
        if (updatedCountLike[id] > 0) {
          updatedCountLike[id] -= 1;
        }
        return updatedCountLike;
      });
    }
  }

  const renderItem = (data) => {
    return data.map((item, id) => (
      <TouchableOpacity onPress={() => onPressAll(id)} style={ComponentStyles(id).TouchableItem}>
        <Text style={ComponentStyles(id).LabelItem}>{item.label}</Text>
      </TouchableOpacity>
    ))
  }

  const renderContent = (data) => {
    return data.map((item, id) => (
      <View key={id} style={ComponentStyles(id).ContentContainer}>
        <View >
          <Image source={item.image} style={ComponentStyles(id).ImageContent} />
        </View>
        <View style={ComponentStyles(id).ButtonContainer}>
          <View style={ComponentStyles(id).CountView}>
            <View style={ComponentStyles(id).CountLike}>
              <Text>{countLike[id]} Like</Text>
            </View>
          </View>
          <View style={ComponentStyles(id).ConditionalContainer}>
            <TouchableOpacity onPress={() => likePressed(id)} style={ComponentStyles(id).LikeButton}>
              <Text style={ComponentStyles(id).LikeLabel}>Like</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => dislikePressed(id)} style={ComponentStyles(id).DislikeButton}>
              <Text style={ComponentStyles(id).DislikeLabel}>Dislike</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    ))
  }

  return (
    <SafeAreaView style={AppStyles.SafeArea}>
      <View style={AppStyles.FirstContainer}>
        {renderItem(dataLabel)}
      </View>
      <ScrollView>
        {renderContent(dataContent)}
      </ScrollView>
    </SafeAreaView>
  );
}


export default App;
